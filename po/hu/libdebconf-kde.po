# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2011, 2020.
# Balázs Úr <urbalazs@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-12 00:46+0000\n"
"PO-Revision-Date: 2020-11-25 22:01+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.03.70\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@kubuntu.org"

#: src/DebconfGui.cpp:136
#, kde-format
msgid "Debconf on %1"
msgstr "Debconf: %1"

#: src/DebconfGui.cpp:229
#, kde-format
msgid ""
"<b>Not implemented</b>: The input widget for data type '%1' is not "
"implemented. Will use default of '%2'."
msgstr ""
"<b>Nincs implementálva</b>: A(z) „%1” adattípushoz tartozó beviteli elem "
"nincs implementálva, az alapértelmezett „%2” kerül felhasználásra."

#: tools/main.cpp:82
#, kde-format
msgctxt "@title"
msgid "Debconf KDE"
msgstr "Debconf KDE"

#: tools/main.cpp:84
#, kde-format
msgctxt "@info"
msgid "Debconf frontend based on Qt"
msgstr "Qt alapú Debconf előtétprogram"

#: tools/main.cpp:90
#, kde-format
msgctxt "@info:shell"
msgid "Path to where the socket should be created"
msgstr "A létrehozandó socket elérési útja"

#: tools/main.cpp:91
#, kde-format
msgctxt "@info:shell value name"
msgid "path_to_socket"
msgstr "path_to_socket"

#: tools/main.cpp:95
#, kde-format
msgctxt "@info:shell"
msgid "FIFO file descriptors for communication with Debconf"
msgstr "FIFO-fájlleírók a Debconffal való kommunikációhoz"

#: tools/main.cpp:96
#, kde-format
msgctxt "@info:shell value name"
msgid "read_fd,write_fd"
msgstr "read_fd,write_fd"